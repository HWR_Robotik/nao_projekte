# Nao_Projekte

Projekte zur Nutzung des Naos

# Mit NAO arbeiten

Anschalten:
Nao am Oberkörper aus dem Paket heben und ihn so in die Hocke stellen, dass er ohne weitere Hilfe steht
Stromkabel in die Steckdose stecken und das andere Ende in seinen Rücken
1x auf Naos Brustknopf drücken und 2-3 Minuten warten bis er hochgefahren ist

Programmieren
Nao mit Ethernet Kabel direkt mit dem Laptop verbinden
Choregraphe starten
Neues Projekt öffnen
Schon vorhandenes Projekt öffnen
„Connection“ anklicken und mit local Nao verbinden

Choregraphe installieren:
https://community.ald.softbankrobotics.com/en/resources/software/language/en-gb und für das gewünschte Betriebssystem runterladen
Installieren

Falls die vcomp120.dll fehlt. Datei aus dem Github runterladen und in C:\Windows\System32 abspeichern.

Ausschalten:
Den Knopf auf seiner Brust für ein paar Sekunden drücken

Autonomous life aktivieren/deaktivieren
Den Knopf auf seiner Brust zweimal hintereinander drücken

IP abfragen:
Den Knopf auf seiner Brust einmal drücken
<?xml version="1.0" encoding="UTF-8" ?>
<Package name="FirstTest" format_version="5">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="HelloWorld" src="HelloWorld/HelloWorld.dlg" />
    </Dialogs>
    <Resources>
        <File name="Twinkle_Twinkle_Little_Star" src="Twinkle_Twinkle_Little_Star.mp3" />
        <File name="Eminem - Rap God (Explicit)" src="Eminem - Rap God (Explicit).mp3" />
        <File name="Linkin Park numb" src="Linkin Park numb.mp3" />
        <File name="NF - Lie (Audio)" src="NF - Lie (Audio).mp3" />
        <File name="mikhael-landscape-paisaje" src="behavior_1/sounds/mikhael-landscape-paisaje.ogg" />
        <File name="heaven1" src="behavior_1/behavior_1/heaven1.ogg" />
        <File name="camera1" src="behavior_1/camera1.ogg" />
        <File name="epicsax" src="behavior_1/epicsax.ogg" />
    </Resources>
    <Topics>
        <Topic name="HelloWorld_enu" src="HelloWorld/HelloWorld_enu.top" topicName="HelloWorld" language="en_US" nuance="enu" />
    </Topics>
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>

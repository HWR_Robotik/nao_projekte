<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Nao_Music" format_version="5">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="epicsax" src="behavior_1/epicsax.ogg" />
        <File name="blackbear - do re mi ft. Gucci Mane" src="blackbear - do re mi ft. Gucci Mane.mp3" />
        <File name="blackbear - idfc (Lyrics)" src="blackbear - idfc (Lyrics).mp3" />
        <File name="blackbear - me  ur ghost" src="blackbear - me  ur ghost.mp3" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>

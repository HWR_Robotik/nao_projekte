<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You are happy!</source>
            <comment>Text</comment>
            <translation type="unfinished">You are happy!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You are surprised!</source>
            <comment>Text</comment>
            <translation type="unfinished">You are surprised!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (2)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You are angry!</source>
            <comment>Text</comment>
            <translation type="unfinished">You are angry!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (3)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You are sad!</source>
            <comment>Text</comment>
            <translation type="unfinished">You are sad!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (4)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You are neutral</source>
            <comment>Text</comment>
            <translation type="unfinished">You are neutral</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (5)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You are a male!</source>
            <comment>Text</comment>
            <translation type="unfinished">You are a male!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (6)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You are a female!</source>
            <comment>Text</comment>
            <translation type="unfinished">You are a female!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (7)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Your age is </source>
            <comment>Text</comment>
            <translation type="unfinished">Your age is </translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Animated Say (8)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Sorry, I can't calculate your age</source>
            <comment>Text</comment>
            <translation type="unfinished">Sorry, I can't calculate your age</translation>
        </message>
    </context>
</TS>
